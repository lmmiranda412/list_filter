defmodule ListFilter do
  def call(list) do
    filter(list, 0)
  end

  defp filter([head | tail], counter) do
    result = Integer.parse(head)
    handle_integer_result(result, tail, counter)
  end

  defp filter([], counter) do
    counter
  end

  defp handle_integer_result(:error, tail, counter) do
    filter(tail, counter)
  end

  defp handle_integer_result({number, _base}, tail, counter) do
    result = rem(number, 2)
    handle_rem_result(result, tail, counter)
  end

  defp handle_rem_result(1, tail, counter) do
    counter = counter + 1
    filter(tail, counter)
  end

  defp handle_rem_result(_number, tail, counter) do
    filter(tail, counter)
  end
end
